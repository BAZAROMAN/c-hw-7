﻿
#include <iostream>
using namespace std;


class animal
{



public:

    virtual void Voice()
    {
        cout << "makes some sound" << "\n";

    }
};
class cat : public animal
{

public:

    void Voice() override
    {
        cout << "meow!" << "\n";

    }
};
class elephant : public animal
{

public:

    void Voice() override
    {
        cout << "u-u-u-u-u!" << "\n";

    }
};
class dog : public animal
{

public:

    void Voice() override

    {
        cout << "Woof!" << "\n";

    }
};


int main()
{

    //  -
    animal* pMaksim = new dog;
    animal* pPolina = new cat;
    animal* pSergey = new elephant;
 
    pMaksim->Voice();
    pSergey->Voice();
    pPolina->Voice();
    cout << "==============================" << "\n";

    animal* pArray[] = { new dog, new cat, new elephant };


    for (int i = 0; i < 3; i++)
    {
        pArray[i]->Voice();
        delete pArray[i];
    }


    return 0;
}
